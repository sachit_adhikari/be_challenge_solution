from .models import Employee, Shift, Schedule

from rest_framework import serializers


class ShiftSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shift
        fields = ('id', 'date', 'from_time', 'to_time', 'break_time')


class ScheduleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Schedule
        fields = ('id', 'employee', 'shift',)


class EmployeeSerializer(serializers.ModelSerializer):
    schedules = ScheduleSerializer(many=True, read_only=True)

    class Meta:
        model = Employee
        fields = ('id', 'name', 'address', 'phone_number', 'schedules',)