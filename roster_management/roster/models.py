from django.db import models


class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Employee(TimeStampedModel):
    """
    Table to store all the details about employee
    """
    name = models.CharField(max_length=250)
    address = models.CharField(max_length=255)
    phone_number = models.IntegerField()  # only aussie number for now

    def schedules(self):
        return Schedule.objects.filter(employee=self)

    def __str__(self):
        return self.name


class Shift(TimeStampedModel):
    date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()
    break_time = models.IntegerField()

    def __str__(self):
        return '{} - {} - {}'.format(self.date.strftime('%A, %D'),
                                     self.from_time, self.to_time)


class Schedule(TimeStampedModel):
    employee = models.ForeignKey(Employee, related_name='schedules',
                                 on_delete=models.CASCADE)
    shift = models.ForeignKey(Shift, related_name='shifts',
                              on_delete=models.CASCADE)

    class Meta:
        unique_together = ('employee', 'shift')

    def __str__(self):
        return self.employee.name
