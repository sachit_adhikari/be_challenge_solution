from django.contrib import admin

from .models import Employee, Shift, Schedule


class ScheduleInline(admin.TabularInline):
    model = Schedule
    extra = 5


class ShiftAdmin(admin.ModelAdmin):
    inlines = (ScheduleInline, )


admin.site.register(Employee)
admin.site.register(Shift, ShiftAdmin)
