## Rooster Management 

### Database

I have followed the three tables approach:

Employeee - to store employee details like name, address etc
Shift - Independent model where we will feed all the shifts data (may be upto 10 years)
Schedule - This is where you attach employee to a shift

The reason for creating a schedule table is solely for the reporting purpose. Sooner or latter we will be asked to report on the employee shifts. How many days they worked, how many leaves they have taken and things like that.


### API

I have used django-rest-framework to create APIs. The Employee Endpoint will give all details about employees and all the schedules associated with the employees.

### Create Schedule Automatically

This is the part I haven't got a chance to implement yet within the 2 hours timeframe. I was thinking of introducing couple of restrictions so that there won't be overlap of the schedule for the employees. Here are the rules:

- Don't allow to create a schedule for same employee on the same day.
- Same day won't be calculated just by the day. Meaning if the shift is 9pm to 5:30 am the day will be currentday + 1 so the first logic doesn't fail.

I haven't given it a deep thought to be honest, but this is what I have on top of my head.

Thanks